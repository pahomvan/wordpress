/* Образцы нативных SQL-запросов */

-- CATEGORY -------------------------------------------------------------------

/* Все рубрики сайта с подсчетом количества статей в них.
Сортировка по количеству статей по убыванию. */
SELECT
  wp_term_taxonomy.count AS posts_count,
  wp_terms.term_id,
  wp_terms.name,
  wp_terms.slug
FROM wp_term_taxonomy
  LEFT JOIN wp_terms USING (term_id)
WHERE wp_term_taxonomy.taxonomy = 'category'
ORDER BY posts_count DESC;


/* Все рубрики сайта, где количество статей больше 10.
С подсчетом количества статей в них.
Сортировка по количеству статей по убыванию. */
SELECT
  wp_term_taxonomy.count AS posts_count,
  wp_terms.term_id,
  wp_terms.name,
  wp_terms.slug
FROM wp_term_taxonomy
  LEFT JOIN wp_terms USING (term_id)
WHERE wp_term_taxonomy.taxonomy = 'category'
  AND wp_term_taxonomy.count > 10
ORDER BY posts_count DESC;


/* Все рубрики сайта в виде одной строки */
SELECT
  GROUP_CONCAT(
      DISTINCT wp_terms.name ORDER BY wp_terms.name ASC SEPARATOR ', '
  ) as list
FROM wp_term_taxonomy
  LEFT JOIN wp_terms USING (term_id)
WHERE wp_term_taxonomy.taxonomy = 'category';


/* Информация по конкретной рубрике */
SELECT
  wp_terms.name,
  wp_terms.slug,
  wp_term_taxonomy.description
FROM wp_term_taxonomy
  LEFT JOIN wp_terms USING (term_id)
WHERE wp_term_taxonomy.taxonomy = 'category'
  AND wp_terms.slug = '<NAME>';

-- POST -------------------------------------------------------------------

/* Последние статьи */
SELECT
  wp_posts.post_title as title,
  wp_posts.post_name as link,
  wp_posts.post_date
FROM wp_posts
WHERE wp_posts.post_type = 'post'
  AND wp_posts.post_status = 'publish'
ORDER BY wp_posts.post_date DESC
LIMIT 5;


/* Все статьи конкретной рубрики.
Обратная сортировка по дате создания. */
SELECT
  wp_terms.name, wp_terms.slug,
  wp_term_taxonomy.term_id, wp_term_relationships.object_id,
  wp_posts.post_title, wp_posts.post_name, wp_posts.post_content,
  wp_posts.post_date, wp_users.user_nicename,
  wp_term_taxonomy.description
FROM wp_terms
  LEFT JOIN wp_term_taxonomy USING (term_id)
  LEFT JOIN wp_term_relationships USING (term_taxonomy_id)
  LEFT JOIN wp_posts ON wp_term_relationships.object_id = wp_posts.ID
  LEFT JOIN wp_users ON wp_users.ID = wp_posts.post_author
WHERE wp_terms.slug = '<SLUG>'
  AND wp_posts.post_type = 'post'
  AND wp_posts.post_status = 'publish'
  AND wp_term_taxonomy.taxonomy = 'category'
ORDER BY wp_posts.post_date DESC;


/* Все категории, к которым относится конкретная статья */
SELECT
  wp_terms.name,
  wp_terms.slug
FROM wp_posts
  LEFT JOIN wp_term_relationships
    ON wp_term_relationships.object_id = wp_posts.ID
  LEFT JOIN wp_term_taxonomy USING (term_taxonomy_id)
  LEFT JOIN wp_terms USING (term_id)
WHERE wp_term_taxonomy.taxonomy = 'category'
      AND wp_posts.post_name = '<NAME>';


/* Ссылки pre-next для конкретной статьи */
(
  SELECT
    wp_posts.post_title,
    wp_posts.post_name,
    'prev'
  FROM wp_posts
  WHERE wp_posts.post_status = 'publish'
    AND wp_posts.post_date <
        (
          SELECT
            wp_posts.post_date
          FROM wp_posts
          WHERE wp_posts.post_name = '<NAME>'
        )
  ORDER BY wp_posts.post_date DESC LIMIT 1
)
UNION
(
  SELECT
    wp_posts.post_title,
    wp_posts.post_name,
    'next'
  FROM wp_posts
  WHERE wp_posts.post_status = 'publish'
    AND wp_posts.post_date >
        (
          SELECT
            wp_posts.post_date
          FROM wp_posts
          WHERE post_name = '<NAME>'
        )
  ORDER BY wp_posts.post_date ASC LIMIT 1
);


-- PAGE -------------------------------------------------------------------

/* Последние 5 страниц.
 Обратная сортировка по дате. */
SELECT
  wp_posts.post_title as title,
  wp_posts.post_name as link,
  wp_posts.post_date
FROM wp_posts
WHERE wp_posts.post_type = 'page'
  AND wp_posts.post_status = 'publish'
ORDER BY wp_posts.post_date DESC
LIMIT 5;


/* Дочерние страницы для конкретной.
 Обратная сортировка по menu_order. */
SELECT *
FROM wp_posts
WHERE wp_posts.post_type = 'page'
  AND wp_posts.post_status = 'publish'
  AND wp_posts.post_parent =
    (
      SELECT wp_posts.ID
      FROM wp_posts
      WHERE wp_posts.post_name = '<NAME>'
    )
ORDER BY wp_posts.menu_order DESC;


/* Все страницы имеющие дочерние страницы
 - то есть являющиеся разделами сайта. */
SELECT
  wpp.post_title,
  wpp.post_name,
  wpp.ID
FROM wp_posts wpp
WHERE wpp.ID IN
  (
    SELECT wp_posts.post_parent
    FROM wp_posts
    WHERE wp_posts.post_type = 'page'
      AND wp_posts.post_parent != 0
    GROUP BY wp_posts.post_parent
  )
  AND wpp.post_parent = 0
GROUP BY wpp.post_title;


-- TAG -------------------------------------------------------------------

/* Все метки/теги сайта с подсчетом количества статей по ним.
Сортировка по количеству статей по убыванию. */
SELECT
  wp_term_taxonomy.count AS posts_count,
  wp_terms.term_id,
  wp_terms.name,
  wp_terms.slug
FROM wp_term_taxonomy
  LEFT JOIN wp_terms USING (term_id)
WHERE wp_term_taxonomy.taxonomy = 'post_tag'
ORDER BY posts_count DESC;


/* Все статьи по конкретному тегу */
SELECT
  wp_terms.name, wp_terms.slug,
  wp_term_taxonomy.term_id,
  wp_term_relationships.object_id,
  wp_posts.post_title,
  wp_posts.post_name
FROM wp_terms
  LEFT JOIN wp_term_taxonomy USING (term_id)
  LEFT JOIN wp_term_relationships USING (term_taxonomy_id)
  LEFT JOIN wp_posts ON wp_term_relationships.object_id = wp_posts.ID
WHERE wp_terms.slug = '<TAG>'
  AND wp_term_taxonomy.taxonomy = 'post_tag'
ORDER BY wp_posts.post_date DESC;



-- ARCHIEVE -------------------------------------------------------------------

/* Все архивы по годам */
SELECT DISTINCT
  SUBSTRING(wp_posts.post_date, 1, 4) AS year
FROM wp_posts
ORDER BY year DESC;


/* Все архивы по годам и месяцам */
SELECT DISTINCT
  SUBSTRING(wp_posts.post_date, 1, 7) AS year
FROM wp_posts
ORDER BY year DESC;


/* Все посты за определенный год */
SELECT *
FROM wp_posts
WHERE wp_posts.post_date LIKE '<YEAR>%'
  AND wp_posts.post_type = 'post'
  AND wp_posts.post_status = 'publish'
ORDER BY wp_posts.post_date ASC;


/* Все посты за определенный год и месяц */
SELECT *
FROM wp_posts
WHERE wp_posts.post_date LIKE '<YEAR>-<MONTH>%'
  AND wp_posts.post_type = 'post'
  AND wp_posts.post_status = 'publish'
ORDER BY wp_posts.post_date ASC;


-- AUTHOR -------------------------------------------------------------------

/* Все статьи конкретного автора.
 Обратная сортировка по дате создания. */
SELECT
  wp_posts.*
FROM wp_posts
  LEFT JOIN wp_users ON wp_users.ID = wp_posts.post_author
WHERE wp_users.user_nicename = '<AUTOR>'
  AND wp_posts.post_type = 'post'
  AND wp_posts.post_status = 'publish'
ORDER BY wp_posts.post_date DESC;


/* Подсчет статей по каждому из авторов.
 Обратная сортировка по количеству статей. */
SELECT
  COUNT(*) as count_posts,
  wp_users.user_nicename
FROM wp_posts
  LEFT JOIN wp_users ON wp_users.ID = wp_posts.post_author
WHERE wp_posts.post_type = 'post'
  AND wp_posts.post_status = 'publish'
GROUP BY wp_users.user_nicename
ORDER BY count_posts DESC;


/* Подсчет статей по каждому из авторов.
 Для тех, у кого количество статей больше 200.
 Обратная сортировка по количеству статей. */
SELECT
  COUNT(*) as count_posts,
  wp_users.user_nicename
FROM wp_posts
  LEFT JOIN wp_users ON wp_users.ID = wp_posts.post_author
WHERE wp_posts.post_type = 'post'
  AND wp_posts.post_status = 'publish'
GROUP BY wp_users.user_nicename
HAVING count_posts > 200
ORDER BY count_posts DESC;
