# wordpress
Work with Wordpress: native / yii2

[![Latest Stable Version](https://poser.pugx.org/commercito/wordpress/version)](https://packagist.org/packages/commercito/wordpress)
[![Total Downloads](https://poser.pugx.org/commercito/wordpress/downloads)](https://packagist.org/packages/commercito/wordpress)
[![License](https://poser.pugx.org/commercito/wordpress/license)](https://packagist.org/packages/commercito/wordpress)

- native sql queries examples
- trait for your classes
- ActiveRecord models for Yii2
- common selector for controllers
- hasOne/ hasMany trait for selector

***
read the [WIKI](https://gitlab.com/commercito/wordpress/wikis/Главная)
***
https://packagist.org/packages/commercito/wordpress