<?php
/**
 * Created at: 07.04.2018 21:29
 * @author commercito <commercito@gmail.com>
 * @link http://commercito.ru/
 * @copyright Copyright (c) 2018 commercito
 */

namespace commercito\wordpress\native;

/**
 * Native sql queries for Wordpress Database
 *
 * @package commercito\wordpress\native
 */
trait NativeSqlTrait
{
    /**
     * Table prefix
     * @var string
     */
    protected $tablePrefix = "wp_";

    /**
     * Get all the categories of the site with the count of the number of articles in them.
     * Sort by number of articles in descending order.
     * @return string
     */
    public function sqlCategoryAllCount()
    {
        $sql = <<<SQL
SELECT
  {$this->tablePrefix}term_taxonomy.count AS posts_count,
  {$this->tablePrefix}terms.term_id,
  {$this->tablePrefix}terms.name,
  {$this->tablePrefix}terms.slug
FROM {$this->tablePrefix}term_taxonomy
  LEFT JOIN {$this->tablePrefix}terms USING (term_id)
WHERE {$this->tablePrefix}term_taxonomy.taxonomy = 'category'
ORDER BY posts_count DESC;
SQL;
        return $sql;
    }

    /**
     * Get all the categories of the site, where the number of articles is more than $moreThan.
     * Counting the number of articles in them.
     * Sort by number of articles in descending order.
     * @param int $moreThan | default = 10
     * @return string
     */
    public function sqlCategoryAllCountMoreThan($moreThan=10)
    {
        $sql = <<<SQL
SELECT
  {$this->tablePrefix}term_taxonomy.count AS posts_count,
  {$this->tablePrefix}terms.term_id,
  {$this->tablePrefix}terms.name,
  {$this->tablePrefix}terms.slug
FROM {$this->tablePrefix}term_taxonomy
  LEFT JOIN {$this->tablePrefix}terms USING (term_id)
WHERE {$this->tablePrefix}term_taxonomy.taxonomy = 'category'
  AND {$this->tablePrefix}term_taxonomy.count > {$moreThan}
ORDER BY posts_count DESC;
SQL;
        return $sql;
    }

    /**
     * Get information on a specific category
     * @param $slug
     * @return string
     */
    public function sqlCategoryInfo($slug)
    {
        $sql = <<<SQL
SELECT
  {$this->tablePrefix}terms.name,
  {$this->tablePrefix}terms.slug,
  {$this->tablePrefix}term_taxonomy.description
FROM {$this->tablePrefix}term_taxonomy
  LEFT JOIN {$this->tablePrefix}terms USING (term_id)
WHERE {$this->tablePrefix}term_taxonomy.taxonomy = 'category'
  AND {$this->tablePrefix}terms.slug = '{$slug}';
SQL;
        return $sql;
    }

    /**
     * Get the Latest Posts in the right amount.
     * Reverse sort by creation date.
     * @param $limit
     * @return string
     */
    public function sqlPostLast($limit)
    {
        $sql = <<<SQL
SELECT
  {$this->tablePrefix}posts.post_title as title,
  {$this->tablePrefix}posts.post_name as link,
  {$this->tablePrefix}posts.post_date
FROM {$this->tablePrefix}posts
WHERE {$this->tablePrefix}posts.post_type = 'post'
  AND {$this->tablePrefix}posts.post_status = 'publish'
ORDER BY {$this->tablePrefix}posts.post_date DESC
LIMIT {$limit};
SQL;
        return $sql;
    }

    /**
     * Get All articles of a specific category.
     * Reverse sort by creation date.
     * @param $categorySlug
     * @return string
     */
    public function sqlPostFromCategory($categorySlug)
    {
        $sql = <<<SQL
SELECT
  {$this->tablePrefix}terms.name, {$this->tablePrefix}terms.slug,
  {$this->tablePrefix}term_taxonomy.term_id, {$this->tablePrefix}term_relationships.object_id,
  {$this->tablePrefix}posts.post_title, {$this->tablePrefix}posts.post_name, {$this->tablePrefix}posts.post_content,
  {$this->tablePrefix}posts.post_date, {$this->tablePrefix}users.user_nicename,
  {$this->tablePrefix}term_taxonomy.description
FROM {$this->tablePrefix}terms
  LEFT JOIN {$this->tablePrefix}term_taxonomy USING (term_id)
  LEFT JOIN {$this->tablePrefix}term_relationships USING (term_taxonomy_id)
  LEFT JOIN {$this->tablePrefix}posts ON {$this->tablePrefix}term_relationships.object_id = {$this->tablePrefix}posts.ID
  LEFT JOIN {$this->tablePrefix}users ON {$this->tablePrefix}users.ID = {$this->tablePrefix}posts.post_author
WHERE {$this->tablePrefix}terms.slug = '{$categorySlug}'
  AND {$this->tablePrefix}terms.post_type = 'post'
  AND {$this->tablePrefix}posts.post_status = 'publish'
  AND {$this->tablePrefix}term_taxonomy.taxonomy = 'category'
ORDER BY post_date DESC;
SQL;
        return $sql;
    }

    /**
     * Get all categories to which a particular post applies
     * @param $postName
     * @return string
     */
    public function sqlPostParentCategory($postName)
    {
        $sql = <<<SQL
SELECT
  {$this->tablePrefix}terms.name,
  {$this->tablePrefix}terms.slug
FROM {$this->tablePrefix}posts
  LEFT JOIN {$this->tablePrefix}term_relationships
    ON {$this->tablePrefix}term_relationships.object_id = {$this->tablePrefix}posts.ID
  LEFT JOIN {$this->tablePrefix}term_taxonomy USING (term_taxonomy_id)
  LEFT JOIN {$this->tablePrefix}terms USING (term_id)
WHERE {$this->tablePrefix}term_taxonomy.taxonomy = 'category'
      AND {$this->tablePrefix}posts.post_name = '{$postName}';
SQL;
        return $sql;
    }

    /**
     * Get prev-next links for a specific article
     * @param $postName
     * @return string
     */
    public function sqlPostPrevNextLink($postName)
    {
        $sql = <<<SQL
(
  SELECT
    {$this->tablePrefix}posts.post_title, 
    {$this->tablePrefix}posts.post_name, 'prev'
  FROM {$this->tablePrefix}posts
  WHERE {$this->tablePrefix}posts.post_status = 'publish'
    AND {$this->tablePrefix}posts.post_date <
        (
          SELECT
            post_date
          FROM {$this->tablePrefix}posts
          WHERE {$this->tablePrefix}posts.post_name = '{$postName}'
        )
  ORDER BY {$this->tablePrefix}post_date{$this->tablePrefix} DESC LIMIT 1
)
UNION
(
  SELECT 
    {$this->tablePrefix}posts.post_title, 
    {$this->tablePrefix}posts.post_name, 'next'
  FROM {$this->tablePrefix}posts
  WHERE {$this->tablePrefix}posts.post_status = 'publish'
    AND {$this->tablePrefix}posts.post_date >
        (
          SELECT
            {$this->tablePrefix}posts.post_date
          FROM {$this->tablePrefix}posts
          WHERE {$this->tablePrefix}posts.post_name = '{$postName}'
        )
  ORDER BY {$this->tablePrefix}posts.post_date ASC LIMIT 1
);
SQL;
        return $sql;
    }

    /**
     * Get child pages for a specific.
     * Reverse sorting by menu_order.
     * @param $pageName
     * @return string
     */
    public function sqlPageGetChild($pageName)
    {
        $sql = <<<SQL
SELECT *
FROM {$this->tablePrefix}posts
WHERE {$this->tablePrefix}posts.post_type = 'page'
  AND {$this->tablePrefix}posts.post_status = 'publish'
  AND {$this->tablePrefix}posts.post_parent =
    (
      SELECT ID
      FROM {$this->tablePrefix}posts
      WHERE {$this->tablePrefix}posts.post_name = '{$pageName}'
    )
ORDER BY {$this->tablePrefix}posts.menu_order DESC;
SQL;
        return $sql;
    }

    /**
     * Get all pages that have child pages
     * - that is, they are sections of the site.
     * @return string
     */
    public function sqlPageHaveChild()
    {
        $sql = <<<SQL
SELECT
  wpp.post_title,
  wpp.post_name,
  wpp.ID
FROM {$this->tablePrefix}posts wpp
WHERE wpp.ID IN
    (
      SELECT post_parent
      FROM {$this->tablePrefix}posts
      WHERE {$this->tablePrefix}posts.post_type = 'page'
            AND {$this->tablePrefix}posts.post_parent != 0
      GROUP BY {$this->tablePrefix}posts.post_parent
    )
    AND wpp.post_parent = 0
GROUP BY wpp.post_title;
SQL;
        return $sql;
    }

    /**
     * Get all tags of the site with counting the number of articles on them.
     * Sort by number of posts in descending order.
     * @return string
     */
    public function sqlTagAllCount()
    {
        $sql = <<<SQL
SELECT
  {$this->tablePrefix}term_taxonomy.count AS posts_count,
  {$this->tablePrefix}terms.term_id,
  {$this->tablePrefix}terms.name,
  {$this->tablePrefix}terms.slug
FROM {$this->tablePrefix}term_taxonomy
  LEFT JOIN {$this->tablePrefix}terms USING (term_id)
WHERE {$this->tablePrefix}term_taxonomy.taxonomy = 'post_tag'
ORDER BY posts_count DESC;
SQL;
        return $sql;
    }

    /**
     * Get all posts for a specific tag
     * @param $slug
     * @return string
     */
    public function sqlPostFromTag($slug)
    {
        $sql = <<<SQL
SELECT
  {$this->tablePrefix}terms.name, {$this->tablePrefix}terms.slug,
  {$this->tablePrefix}term_taxonomy.term_id,
  {$this->tablePrefix}term_relationships.object_id,
  {$this->tablePrefix}posts.post_title,
  {$this->tablePrefix}posts.post_name
FROM {$this->tablePrefix}terms
  LEFT JOIN {$this->tablePrefix}term_taxonomy USING (term_id)
  LEFT JOIN {$this->tablePrefix}term_relationships USING (term_taxonomy_id)
  LEFT JOIN {$this->tablePrefix}posts ON {$this->tablePrefix}term_relationships.object_id = {$this->tablePrefix}posts.ID
WHERE {$this->tablePrefix}terms.slug = '{$slug}' 
  AND {$this->tablePrefix}terms.post_status = 'publish'
  AND {$this->tablePrefix}term_taxonomy.taxonomy = 'post_tag'
ORDER BY {$this->tablePrefix}posts.post_date DESC;
SQL;
        return $sql;
    }

    /**
     * Get all archives by years
     * @return string
     */
    public function sqlArchiveYear()
    {
        $sql = <<<SQL
SELECT DISTINCT
  SUBSTRING({$this->tablePrefix}posts.post_date, 1, 4) AS year
FROM {$this->tablePrefix}posts
ORDER BY year DESC;
SQL;
        return $sql;
    }

    /**
     * Get all archives by year and month
     * @return string
     */
    public function sqlArchiveYearMonth()
    {
        $sql = <<<SQL
SELECT DISTINCT
  SUBSTRING({$this->tablePrefix}posts.post_date, 1, 7) AS year
FROM {$this->tablePrefix}posts
ORDER BY year DESC;
SQL;
        return $sql;
    }

    /**
     * Get all posts for a certain year
     * @param $year
     * @return string
     */
    public function sqlPostFromYear($year)
    {
        $sql = <<<SQL
SELECT *
FROM {$this->tablePrefix}posts
WHERE {$this->tablePrefix}posts.post_date LIKE '{$year}%'
  AND {$this->tablePrefix}posts.post_type = 'post'
  AND {$this->tablePrefix}posts.post_status = 'publish'
ORDER BY {$this->tablePrefix}posts.post_date ASC;
SQL;
        return $sql;
    }

    /**
     * Get all posts for a specific year and month
     * @param $year
     * @param $month
     * @return string
     */
    public function sqlPostFromYearMonth($year,$month)
    {
        $sql = <<<SQL
SELECT *
FROM {$this->tablePrefix}posts
WHERE {$this->tablePrefix}posts.post_date LIKE '{$year}-{$month}%'
  AND {$this->tablePrefix}posts.post_type = 'post'
  AND {$this->tablePrefix}posts.post_status = 'publish'
ORDER BY {$this->tablePrefix}posts.post_date ASC;
SQL;
        return $sql;
    }

    /**
     * Get all the posts of a specific author.
     * Reverse sort by creation date.
     * @param $author
     * @return string
     */
    public function sqlAuthorAllPost($author)
    {
        $sql = <<<SQL
SELECT
  {$this->tablePrefix}posts.*
FROM {$this->tablePrefix}posts
  LEFT JOIN {$this->tablePrefix}users ON {$this->tablePrefix}users.ID = {$this->tablePrefix}posts.post_author
WHERE {$this->tablePrefix}users.user_nicename = '$author'
  AND {$this->tablePrefix}posts.post_type = 'post'
  AND {$this->tablePrefix}posts.post_status = 'publish'
ORDER BY {$this->tablePrefix}posts.post_date DESC;
SQL;
        return $sql;
    }

    /**
     * Counting of articles for each of the authors.
     * Reverse sorting by the number of articles.
     * @return string
     */
    public function sqlAuthorCountPost()
    {
        $sql = <<<SQL
SELECT
  COUNT(*) as count_posts,
  {$this->tablePrefix}users.user_nicename
FROM {$this->tablePrefix}posts
  LEFT JOIN {$this->tablePrefix}users ON {$this->tablePrefix}users.ID = {$this->tablePrefix}posts.post_author
WHERE {$this->tablePrefix}posts.post_type = 'post'
  AND {$this->tablePrefix}posts.post_status = 'publish'
GROUP BY {$this->tablePrefix}users.user_nicename
ORDER BY count_posts DESC;
SQL;
        return $sql;
    }

    /**
     * @return string
     */
    public function getTablePrefix()
    {
        return $this->tablePrefix;
    }

    /**
     * @param string $tablePrefix
     */
    public function setTablePrefix($tablePrefix)
    {
        $this->tablePrefix = $tablePrefix;
    }
}