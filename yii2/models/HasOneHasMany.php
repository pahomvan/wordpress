<?php
/**
 * Created at: 29.04.2018 17:07
 * @author commercito <commercito@gmail.com>
 * @link http://commercito.ru/
 * @copyright Copyright (c) 2018 commercito
 */

namespace commercito\wordpress\yii2\models;

/**
 * Trait for using hasOne and hasMany
 * from ->with(['key1','key2', etc])
 *
 * @package frontend\models
 */
trait HasOneHasMany
{
    /**
     * Required meta keys
     * @var array
     */
    public static $needMetaKey = [
        'fromhere',
        'ogimg',
        'keywords',
        'price',
        'litres',
        'banner'
    ];

    /**
     * Get post author = display_name
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->hasOne(WpUsers::className(), ['ID' => 'post_author'])
            ->select([
                '{{%users}}.display_name',
                '{{%users}}.ID'
            ]);
    }

    /**
     * Get all post tags
     * @return mixed
     */
    public function getTags()
    {
        $array = $this->hasMany(WpTermRelationships::className(), ['object_id' => 'ID'])
            ->select([
                '{{%term_relationships}}.object_id',
                '{{%terms}}.name',
                '{{%terms}}.slug'
            ])
            ->leftJoin(
                '{{%term_taxonomy}}',
                    '{{%term_taxonomy}}.term_taxonomy_id = {{%term_relationships}}.term_taxonomy_id')
            ->leftJoin(
                '{{%terms}}',
                    '{{%terms}}.term_id = {{%term_taxonomy}}.term_id')
            ->where(['{{%term_taxonomy}}.taxonomy'=>'post_tag']);
        return $array;
    }

    /**
     * Get post meta information
     * need meta_key and meta_value
     * from wp_postmeta tble
     * @return mixed
     */
    public function getMeta()
    {
        $array = $this->hasMany(WpPostmeta::className(), ['post_id' => 'ID'])
            ->where([
                'meta_key'=>self::$needMetaKey
            ]);
        return $array;
    }
}