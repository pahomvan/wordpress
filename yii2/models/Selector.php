<?php
/**
 * Created at: 29.04.2018 14:56
 * @author commercito <commercito@gmail.com>
 * @link http://commercito.ru/
 * @copyright Copyright (c) 2018 commercito
 */

namespace commercito\wordpress\yii2\models;

/**
 * Class to retrieve from the Wordpress database
 *
 * @package frontend\models
 */
class Selector extends \yii\db\ActiveRecord
{
    use HasOneHasMany;

    public static $searchPhrase = false;
    public static $substrPostContent = false;

    /**
     * Get all posts for current author
     * @param string $nickname
     * @param bool $substrChars
     * @return mixed
     */
    public static function getAuthorPosts($nickname, $substrChars=false)
    {
        self::$substrPostContent = $substrChars;
        $select = self::selectFromPosts()
            ->leftJoin(
                '{{%users}}',
                '{{%users}}.ID = {{%posts}}.post_author')
            ->andWhere("{{%users}}.user_nicename = '{$nickname}'")
            ->groupBy('{{%posts}}.post_title')
            ->orderBy(['{{%posts}}.post_date' => SORT_DESC])
            ->asArray()
            ->all();
        return $select;
    }

    /**
     * Get actual authors list
     * @return mixed
     */
    public static function getAuthorsList()
    {
        $select = self::find()
            ->select([
                '{{%users}}.display_name author',
                'COUNT({{%posts}}.ID) count_post',
            ])
            ->from([
                '{{%users}}'
            ])
            ->leftJoin(
                '{{%posts}}',
                '{{%posts}}.post_author = {{%users}}.ID'
            )
            ->groupBy('{{%users}}.display_name')
            ->orderBy([
                'count_post' => SORT_DESC
            ])
            ->asArray()
            ->all();
        return $select;
    }

    /**
     * Get posts for current tag
     * @param string $slug
     * @return mixed
     */
    public static function getTagPosts($slug)
    {
        $select = self::find()
            ->select([
                '{{%term_taxonomy}}.term_id',
                '{{%terms}}.name',
                '{{%terms}}.slug',
                '{{%term_relationships}}.object_id',
                '{{%posts}}.ID',
                '{{%posts}}.post_date',
                '{{%posts}}.post_author',
                '{{%posts}}.post_title',
                '{{%posts}}.post_name',
                '{{%posts}}.post_modified',
                '{{%posts}}.post_content',
            ])
            ->from([
                '{{%terms}}'
            ])
            ->leftJoin(
                '{{%term_taxonomy}}',
                '{{%term_taxonomy}}.term_id = {{%terms}}.term_id'
            )
            ->leftJoin(
                '{{%term_relationships}}',
                '{{%term_relationships}}.term_taxonomy_id = {{%term_taxonomy}}.term_taxonomy_id'
            )
            ->leftJoin(
                '{{%posts}}',
                '{{%posts}}.ID = {{%term_relationships}}.object_id'
            )
            ->where([
                '{{%terms}}.slug'=>$slug
            ])
            ->groupBy(['{{%posts}}.post_name'])
            ->orderBy(['{{%posts}}.post_date'=>SORT_DESC])
            ->asArray()
            ->all();
        return $select;
    }

    /**
     * Get all tags with count posts
     * @return mixed
     */
    public static function getTagsList()
    {
        $select = self::find()
            ->select([
                '{{%term_taxonomy}}.count posts_count',
                '{{%terms}}.term_id',
                '{{%terms}}.name',
                '{{%terms}}.slug',
            ])
            ->from([
                '{{%term_taxonomy}}'
            ])
            ->leftJoin(
                '{{%terms}}',
                '{{%term_taxonomy}}.term_id = {{%terms}}.term_id'
            )
            ->where([
                '{{%term_taxonomy}}.taxonomy'=>'post_tag'
            ])
            ->having('posts_count > 0')
            ->orderBy(['count'=>SORT_DESC])
            ->asArray()
            ->all();
        return $select;
    }

    /**
     * What select from wp_posts table
     * @return mixed
     */
    protected static function selectFromPosts()
    {
        $postContent = !self::$substrPostContent
            ? '{{%posts}}.post_content'
            : 'SUBSTRING({{%posts}}.post_content,1,'.self::$substrPostContent.') post_content';
        $select = self::find()
            ->select([
                '{{%posts}}.ID',
                '{{%posts}}.post_date',
                '{{%posts}}.post_author',
                '{{%posts}}.post_title',
                '{{%posts}}.post_name',
                '{{%posts}}.post_modified',
                $postContent
            ])
            ->from([
                '{{%posts}}'
            ])
            ->with(['author','tags','meta'])
            ->where([
                '{{%posts}}.post_status'=>'publish',
                '{{%posts}}.post_type'=>'post',
            ])
            ->andWhere(['not like', '{{%posts}}.post_name', 'autosave']);
        return $select;
    }

    /**
     * Get post lists for search page
     * @param string $phrase
     * @param null $limit
     * @param bool $substrChars
     * @return mixed
     */
    public static function getSearchPosts($phrase,$limit=null,$substrChars=false)
    {
        self::$searchPhrase = $phrase;
        $select = self::getPosts($limit,$substrChars);
        return $select;
    }

    /**
     * Get post lists
     * @param null $limit
     * @param bool $substrChars
     * @return mixed
     */
    public static function getPosts($limit=null,$substrChars=false)
    {
        self::$substrPostContent = $substrChars;
        $select = self::selectFromPosts();
        if (self::$searchPhrase) {
            $select = $select
                ->andWhere(['like', '{{%posts}}.post_content', self::$searchPhrase])
                ->orWhere(['like', '{{%posts}}.post_title', self::$searchPhrase]);
        };
        $select = $select->andWhere(['{{%posts}}.post_status'=>'publish']);
        $select = $select->orderBy(['post_date' => SORT_DESC])
            ->limit($limit)
            ->asArray()
            ->all();
        return $select;
    }

    /**
     * Get one post
     * @param string $link
     * @return mixed
     */
    public static function getPostOne($link)
    {
        $select = self::selectFromPosts()
            ->andWhere([
                '{{%posts}}.post_name'=>$link
            ])
            ->asArray()
            ->one();
        return $select;
    }

    /**
     * Get post for current category by slug
     * @param string $slug
     * @param bool $substrChars
     * @return mixed
     */
    public static function getCategoryPosts($slug, $substrChars=false)
    {
        self::$substrPostContent = $substrChars;
        $select = self::selectFromPosts()
            ->leftJoin(
                '{{%term_relationships}}',
                '{{%posts}}.ID = {{%term_relationships}}.object_id'
            )
            ->leftJoin(
                '{{%term_taxonomy}}',
                '{{%term_taxonomy}}.term_taxonomy_id = {{%term_relationships}}.term_taxonomy_id'
            )
            ->leftJoin(
                '{{%terms}}',
                '{{%term_taxonomy}}.term_id = {{%terms}}.term_id'
            )
            ->andWhere("{{%terms}}.slug = '{$slug}'")
            ->orderBy(['{{%posts}}.post_date' => SORT_DESC])
            ->asArray()
            ->all();
        return $select;
    }

    /**
     * Get one category description
     * @param string $slug
     * @return mixed
     */
    public static function getCategoryAbout($slug)
    {
        $select = self::find()
            ->select('*')
            ->from([
                '{{%terms}}'
            ])
            ->leftJoin(
                '{{%term_taxonomy}}',
                '{{%term_taxonomy}}.term_id = {{%terms}}.term_id'
            )
            ->where(['slug' => $slug])
            ->andWhere(['{{%term_taxonomy}}.taxonomy' => 'category'])
            ->asArray()
            ->one();
        return $select;
    }

    /**
     * Get categories list
     * with name/description/count
     * @param string $count
     * @param int $sort
     * @return mixed
     */
    public static function getCategoriesList($count='>0',$sort=SORT_DESC)
    {
        $select = self::find()
            ->select([
                '{{%term_taxonomy}}.term_id',
                '{{%term_taxonomy}}.parent',
                '{{%term_taxonomy}}.count',
                '{{%term_taxonomy}}.description',
                '{{%terms}}.name',
                '{{%terms}}.slug'
            ])
            ->from([
                '{{%term_taxonomy}}'
            ])
            ->leftJoin(
                '{{%terms}}',
                '{{%term_taxonomy}}.term_id = {{%terms}}.term_id'
            )
            ->where(['taxonomy' => 'category'])
            ->orderBy(['count' => $sort]);
        if ($count) $select = $select->andWhere('count'.$count);
        $select = $select->asArray()->all();
        return $select;
    }


    /**
     * Dummy
     * @return string
     */
    public static function tableName()
    {
        return '{{%posts}}';
    }
}